@tests
Feature: Projects
  - User is able to see newly created project in project list

  Scenario: User is able to see newly created project in project list
    Given I create new project with 'Coca-Cola' name via API call
    And I am on home screen
    When I login to app using email address
    And I tap ['Never ask'] button
    And I tap ['Menu'] button
    And I expand project list
    Then I should see 'Coca-Cola' project
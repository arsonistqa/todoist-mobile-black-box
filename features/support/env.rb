$LOAD_PATH << File.dirname(__FILE__) + '/../lib'

ENVIRONMENT = (ENV['ENVIRONMENT'] || 'production').to_sym
Dir.mkdir('report') unless File.directory?('report')
FileUtils.rm_rf Dir.glob('report/*')
unless File.exist? "#{File.dirname(__FILE__)}/../../lib/config/#{ENVIRONMENT}.yml"
  raise "Create a configuration file '#{ENVIRONMENT}.yml' under lib/config"
end

require 'active_support/all'
require 'appium_capybara'
require 'capybara'
require 'capybara/cucumber'
require 'capybara/dsl'
require 'capybara_helper'
require 'env_config'
require 'faraday'
require 'rspec'
require 'selenium-webdriver'
require 'support/string'
require 'site_prism'
require 'todo'

World(CapybaraHelper)

Dir["#{File.dirname(__FILE__)}/../../lib/screens/*_screen.rb"].each { |r| load r }

Capybara.configure do |config|
  config.default_driver = :appium
  config.javascript_driver = :selenium
  config.default_selector = EnvConfig.get :default_selector
  config.default_max_wait_time = EnvConfig.get :wait_time
  config.match = EnvConfig.get :match
  config.ignore_hidden_elements = EnvConfig.get :ignore_hidden_elements
end

Capybara.register_driver :appium do |app|
  opts = Appium.load_appium_txt file: File.join("#{Dir.pwd}/lib/android", 'appium.txt')
  Appium::Capybara::Driver.new app, opts
end

$capy_driver = Capybara.current_session.driver

Before do
  $VERBOSE = nil
  @@dunit ||= false
  unless @@dunit
    CapybaraHelper.current_time = Time.new.in_time_zone('US/Eastern').strftime('%m%d%H%M')
    CapybaraHelper.random_string = generate_random_string
    @@dunit = true
  end
end

After do |scenario|
  if scenario.failed?
    screenshot = "./report/FAILED_#{scenario.name.tr(' ', '_').gsub(/[^0-9A-Za-z_]/, '')}.png"
    page.driver.save_screenshot(screenshot)
    embed screenshot, 'image/png'
  end
  Capybara.current_session.driver.quit
end

private

def generate_random_string(length = 6)
  string = ''
  chars = ('a'..'z').to_a
  length.times do
    string << chars[rand(chars.length - 1)]
  end
  string
end

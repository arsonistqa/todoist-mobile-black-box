require 'todoist_api'
require 'uuid'

When(/^I add new task with the following data:$/) do |table|
  table.hashes.each do |task|
    name = task[:name]

    @todo.new_task_screen.fill_name unique_value(name) if name
  end
  step "I tap ['Save Task'] button"
end

When(/^I select '(.*)' task/) do |task|
  @todo.task_list_screen.click_on_task(unique_value(task))
end

When(/^I complete task/) do
  @todo.edit_task_screen.click_complete_task_button
end

When(/^I wait for task updates/) do
  step 'I wait 2 seconds'
end

Then(/^I click undo for task/) do
  @todo.task_list_screen.click_undo
end

Then(/^I should get '(.*)' task via API call/) do |task|
  api = TodoistApi.new
  list_items = api.all_items
  expect(list_items).to include(unique_value(task))
end

Then(/^I should( not)? see '(.*)' task/) do |negate, task|
  visible = @todo.task_list_screen.task_visible?(unique_value(task))
  negate ? (visible.should be_falsey) : (visible.should be_truthy)
end

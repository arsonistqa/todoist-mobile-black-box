require 'todoist_api'
require 'uuid'

When(/^I create new project with '(.*)' name via API call/) do |project|
  api = TodoistApi.new
  project = api.add_project(UUID.new.generate, unique_value(project))
  expect(project).not_to be_nil
end

When(/^I expand project list$/) do
  wait_until('menu is not expanded') { @todo.task_list_screen.has_btn_expand_project_list? == true }
  @todo.task_list_screen.btn_expand_project_list.click
  wait_until('project list is not opened') { @todo.task_list_screen.has_btn_manage_projects? == true }
end

When(/^I select '(.*)' project$/) do |project|
  @todo.task_list_screen.click_on_project(unique_value(project))
end

Then(/^I should( not)? see '(.*)' project$/) do |negate, project|
  visible = @todo.task_list_screen.project_visible?(unique_value(project))
  negate ? (visible.should be_falsey) : (visible.should be_truthy)
end

When(/^I tap \['(.*)'\] button$/) do |button|
  case button
  when 'Continue welcome with email'
    @todo.home_screen.click_welcome_continue_email_button
  when 'Continue with email'
    @todo.login_screen.click_continue_with_email
  when 'Never ask'
    @todo.task_list_screen.click_never_ask_button
  when 'Menu'
    @todo.task_list_screen.click_menu_button
  when 'FAB'
    @todo.task_list_screen.click_fab_button
  when 'Save Task'
    @todo.new_task_screen.click_save_task_button
  else
    raise ArgumentError, "message: #{button} is not supported"
  end
end

When(/^I wait (.*) seconds$/) do |time|
  sleep time.to_i
end

When(/^I hide keyboard$/) do
  $capy_driver.appium_driver.hide_keyboard
end

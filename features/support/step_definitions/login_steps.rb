Given(/^I am on home screen$/) do
  @todo = Todo.new
  wait_until('welcome screen is not opened') { @todo.home_screen.has_btn_welcome_continue_email? == true }
end

When(/^I login to app using email address$/) do
  email = EnvConfig.get :user_email
  password = EnvConfig.get :user_password
  step "I tap ['Continue welcome with email'] button"
  @todo.login_screen.login_as(email, password)
end

Feature: Tasks
  - Create task via mobile device and verify via API call if created
  - Reopen task and check if task appears on device

  Scenario: Create task via mobile device and verify via API call if created
    Given I create new project with 'Pepsi' name via API call
    And I am on home screen
    When I login to app using email address
    And I tap ['Never ask'] button
    And I tap ['Menu'] button
    And I expand project list
    Then I should see 'Pepsi' project
    When I select 'Pepsi' project
    And I tap ['FAB'] button
    And I add new task with the following data:
      | name          |
      | Buy new pepsi |
    And I hide keyboard
    Then I should see 'Buy new pepsi' task
    And I wait for task updates
    And I should get 'Buy new pepsi' task via API call

  Scenario: Reopen task and check if task appears on device
    Given I create new project with 'Sprite' name via API call
    And I am on home screen
    When I login to app using email address
    And I tap ['Never ask'] button
    And I tap ['Menu'] button
    And I expand project list
    Then I should see 'Sprite' project
    When I select 'Sprite' project
    And I tap ['FAB'] button
    And I add new task with the following data:
      | name           |
      | Buy new sprite |
    And I hide keyboard
    Then I should see 'Buy new sprite' task
    When I select 'Buy new sprite' task
    And I complete task
    When I click undo for task
    Then I should see 'Buy new sprite' task





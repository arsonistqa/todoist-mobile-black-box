class TodoistApi
  include CapybaraHelper

  require 'faraday'
  require 'httpclient'

  def initialize
    url = (EnvConfig.get :url)
    @td = Faraday.new(url: url) do |faraday|
      faraday.request :url_encoded
      faraday.adapter :httpclient
    end
  end

  def add_project(uuid, name)
    response = @td.post do |req|
      req.params['token'] = EnvConfig.get :token
      req.params['resource_types'] = '["projects"]'
      req.params['commands'] = "[{ \"type\": \"project_add\", \"temp_id\": \"#{uuid}\", \"uuid\": \"#{uuid}\", \"args\": { \"name\": \"#{name}\" } }]"
    end
    response.body
  end

  def all_items
    response = @td.post do |req|
      req.params['token'] = EnvConfig.get :token
      req.params['resource_types'] = '["items"]'
    end
    response.body
  end
end

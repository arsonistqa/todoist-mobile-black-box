module CapybaraHelper
  require 'timeout'
  require 'active_support'
  require 'active_support/core_ext/numeric/time'

  mattr_accessor :current_time
  mattr_accessor :random_string

  def unique_value(value)
    magic_word = 'pre-setup'
    if value.include? magic_word
      value.gsub("#{magic_word} ", '')
    else
      "#{value}#{random_string}"
    end
  end

  def wait_until(message, timeout = Capybara.default_max_wait_time)
    Timeout.timeout(timeout) do
      sleep(0.5) until value = yield
      value
    end
  rescue StandardError
    raise StopIteration.new, "#{message} after #{timeout} seconds"
  end

  def element_visible?(xpath_locator)
    element_present?(xpath_locator) && element_displayed?(xpath_locator)
  end

  private

  def element_present?(xpath_locator)
    page.has_xpath?(xpath_locator)
  end

  def element_displayed?(xpath_locator)
    find(:xpath, xpath_locator).visible?
  end
end

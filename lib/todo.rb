class Todo
  def home_screen
    HomeScreen.new
  end

  def login_screen
    LoginScreen.new
  end

  def task_list_screen
    TaskListScreen.new
  end

  def new_task_screen
    NewTaskScreen.new
  end

  def edit_task_screen
    EditTaskScreen.new
  end
end

class EditTaskScreen < SitePrism::Page
  element :btn_complete_task, 'com.todoist:id/menu_item_complete'

  def click_complete_task_button
    btn_complete_task.click
  end
end

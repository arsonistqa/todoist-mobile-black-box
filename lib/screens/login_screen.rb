class LoginScreen < SitePrism::Page
  include CapybaraHelper

  element :txt_email, 'com.todoist:id/email_exists_input'
  element :txt_password, 'com.todoist:id/log_in_password'
  element :btn_login, 'com.todoist:id/btn_log_in'
  element :btn_continue_with_email, 'com.todoist:id/btn_continue_with_email'

  def login_as(email, password)
    fill_form(email, password)
    click_login_button
  end

  def fill_form(email, password)
    wait_until('login form is not opened') { has_txt_email? == true }
    txt_email.set email
    click_continue_with_email
    wait_until('password field is not displayed') { has_txt_password? == true }
    txt_password.set password
  end

  def click_continue_with_email
    btn_continue_with_email.click
  end

  def click_login_button
    btn_login.click
  end
end

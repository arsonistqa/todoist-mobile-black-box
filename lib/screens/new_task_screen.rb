class NewTaskScreen < SitePrism::Page
  element :txt_name, 'android:id/message'
  element :btn_save_task, 'android:id/button1'

  def click_save_task_button
    btn_save_task.click
  end

  def fill_name(name)
    txt_name.set(name)
  end
end

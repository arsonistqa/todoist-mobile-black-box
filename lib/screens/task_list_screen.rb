class TaskListScreen < SitePrism::Page
  include CapybaraHelper

  element :btn_never_ask, 'android:id/button3'
  element :btn_menu, :xpath, '//android.widget.ImageButton[@content-desc="Change the current view"]'
  element :btn_expand_project_list, :xpath, '(//android.widget.ImageView[@content-desc="Expand/collapse"])[1]'
  element :btn_manage_projects, :xpath, '//android.support.v7.widget.RecyclerView/android.widget.TextView[2]'
  element :btn_fab, 'com.todoist:id/fab'
  element :btn_undo, 'com.todoist:id/snackbar_action'

  def click_fab_button
    btn_fab.click
  end

  def click_menu_button
    btn_menu.click
  end

  def click_never_ask_button
    btn_never_ask.click
  end

  def click_expand_project_list_button
    btn_expand_project_list.click
  end

  def project_name(name)
    "//android.widget.TextView[@text=\"#{name}\"]"
  end

  def project_visible?(name)
    element_present?(project_name(name))
  end

  def click_on_project(name)
    find(:xpath, project_name(name)).click
  end

  def task_name(name)
    "//android.widget.TextView[@text=\"#{name}\"]"
  end

  def task_visible?(name)
    element_present?(task_name(name))
  end

  def click_on_task(name)
    find(:xpath, task_name(name)).click
  end

  def click_undo
    btn_undo.click
  end
end

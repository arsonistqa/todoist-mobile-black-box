class HomeScreen < SitePrism::Page
  element :btn_welcome_continue_email, 'com.todoist:id/btn_welcome_continue_with_email'

  def click_welcome_continue_email_button
    btn_welcome_continue_email.click
  end
end

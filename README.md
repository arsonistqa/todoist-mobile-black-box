## Install ruby, I suggest to use v2.4.3

Instruction for mac (only install ruby) https://gorails.com/setup/osx/10.13-high-sierra

## Install bundler

 In terminal run - ```gem install bundler```

## To install all gems please use the following command

 In terminal run - ```bundle install```
 
## Prepare env for running tests
 
 - start android emulator with the same API version as in ./appium.txt file (API 25 for now)
 
 - start appium server before running tests (I installed appium gui version v.1.8.1)

## To Run tests please use the following command

### 1.Run suite
 
 In terminal run for all features - ```cucumber```

### 2.Run specific feature, e.g. Projects

 In terminal run for login feature - ```cucumber features/projects/projects.feature```
 